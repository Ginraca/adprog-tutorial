# Tutorial 0

# I Got Transported to Another World and All I Can Do is Code with My Advanced Programming Knowledge

Anda membuka mata di sebuah ruangan yang gelap. Hanya sedikit cahaya, tapi cukup untuk melihat sekitar.

Udara terasa dingin dan begitu tenang. Seolah tidak ada angin yang berhembus.
Ketika anda benar-benar bisa melihat, anda melihat sesosok manusia duduk di ujung jalan. Ia tampak gelisah.

Wajahnya menatap anda penuh harap sambil berjalan mendekat. Ia tampak berjalan melalui
sebuah bayangan tembus pandang, tapi anda dapat mendengar suara langkahnya dengan jelas.

Semakin ia mendekat, semakin jelas suara itu terdengar di telinga anda. Ketika ia
semakin mendekat, informasi seolah masuk melalui kepala anda. Ia lalu memberitahu
anda bahwa ia telah memanggil anda ke sini, sebuah dunia yang benar-benar berbeda dengan yang anda kenal.

Dengan gelisah, ia meminta maaf telah merepotkan anda dan ingin anda membantunya. 
Ia memperkenalkan diri sebagai [], sebuah ucapan yang tidak bisa anda pahami.
[] menjelaskan bahwa di dunia paralel ini, ada sebuah bahasa sihir kuno bernama java. Manusia di dunia
ini paham bagaimana menggunakan sihir bernama java ini. Mereka telah membuat banyak hal dengan 
java. Bahkan ada yang berhasil membuat sihir web dengan java ini. 

Salah satu
yang banyak dipakai oleh manusia dunia ini adalah sihir bernama Spring.Mereka membuat web
untuk membantu keseharian. Guild, tempat berkumpulnya para
 adventurer menggunakannya untuk mengatur resource mereka. Namun, di sini lah masalah muncul. Penduduk hanya mampu menggunakannya.
 Hasil dari penggunaan memiliki kualitas mantra yang jelek. Susah di maintain dan apabila si perapal sihir mati, 
 tidak ada yang bisa memahami sistem yang telah dibuatnya.
 Untuk itu, [] meminta bantuan kepada anda untuk membantu mereka memperbaiki sistem yang mereka miliki. 

Anda memahami bahwa sihir di dunia ini adalah program di dunia asal anda. 
Mantra adalah kode dan hasil sihir adalah aplikasi. Dengan pemahaman
Advanced Programming yang anda miliki anda bersedia membantu. Memulai
perjalanan anda di dunia lain dengan pemahaman yang anda miliki...

## Magic of Spring

Sayangnya proses pemanggilan membuat ingatan anda kurang
sempurna. Anda paham jika ada pengetahuan
bernama design pattern yang dapat membantu anda di sini. Hanya saja, anda tidak bisa sepenuhnya mengingat pengetahuan itu.
[] mengatakan dengan berjalannya waktu anda akan mendapatkan kembali pengetahuan itu.  

Ketika anda diperkenalkan oleh [] kepada seorang guild master, ia memperlihatkan kepada anda spring yang mereka punyai.
Mereka menjelaskan mantra-mantra dasar yang dibutuhkan untuk membuat produk dengan spring. 

Aplikasi utama spring terlihat sebagai berikut.

```java
@SpringBootApplication
public class Tutorial0Application {

    public static void main(String[] args) {
        SpringApplication.run(Tutorial0Application.class, args);
    }

}

``` 

Anda bisa melihat bahwa ada beberapa tanda yang disebut sebagai anotasi. Pada bagian tersebut
anda melihat ada anotasi `@SpringBootAplication` yang menandakan bahwa class tersebut adalah main class
spring yang berfungsi untuk menjalankan aplikasi yang telah dibuat.

Dari serpihan ingatan, anda mengingat bagaimana spring di dunia asal anda bekerja.
Ada controller yang memiliki tugas untuk mengurus request dan response. Kedua, model terkait dengan 
logic utama aplikasi. Selanjutnya anda mengenal view untuk menampilkan kepada pengguna.
MVC! Anda merasakan ada sesuatu mengenai kata itu, tapi anda tidak bisa mengingatnya dengan sempurna. 

Untuk saat ini anda tahu bahwa controller yang akan berurusan jika ada request ke aplikasi dan memberikan response kepada request tersebut.
Pada bagian controller terlihat seperti berikut.
```java
@Controller
public class MainController {

    @RequestMapping(method = RequestMethod.GET, value = "/")
    private String home() {
        return "home";
    }

}
```

Suatu class yang termasuk controller diberikan anotasi `@controller` 
Class yang termasuk Controller akan memiliki method-method dengan return type String.
Dalam ingatan, anda paham bahwa string di sini mewakili sebuah nama file .html pada folder resources/templates.
Semua file .html pada folder tersebut akan menjadi tampilan aplikasi.

Pada method home, ia mengembalikan string home, yang merujuk pada home.html pada folder templates. 

Method home sendiri memiliki anotasi `@RequestMapping`. Ingatan anda memperlihatkan kepada anda bahwa
anotasi selain dapat diberikan pada class, dapat diberikan kepada method, dan juga attribute.

`RequestMapping` sendiri memiliki fungsi sebagai yang mengatur request yang akan di-handle. 
Method apa yang akan diurus(POST, GET, PUT, DELETE, dll) dan juga url apa yang akan di-handle oleh method tersebut.
Method home sendiri memiliki `RequestMethod.GET` dan value "/". Ini berarti akses kepada url path "/" dengan method
GET akan diurus oleh method home ini.

Dengan penjelasan yang anda pahami tersebut, anda ingat penggunaan lebih lanjut dari controller terkait dengan url dan method.

```java
 @RequestMapping(method = RequestMethod.GET, value = "/greet")
    private String greetingsWithRequestParam(@RequestParam("name")String name, Model model) {
        model.addAttribute("name", name);
        return "home";
    }
``` 
Anotasi `@RequestParam` adalah untuk menandakan url tersebut dapat ditambahkan "?namaParam=suatu nilai". Hal itu berarti
"/greet" di atas dapat dimasukkan parameter name, menjadi "/greet?name=sesuatu" Nilai di dalam `@ReuqestMapping` merupakan nama param
yang diharapkan ada pada request. Dapat ditambahkan `@RequestMapping(required=false)`  untuk membuat nilai variable tersebut
tidak harus ada pada request. Secara default nilai required adalah true. Dengan demikian method greetingsWithRequestParam butuh parameter name
untuk request ke url tersebut. `String name` merupakan nama parameter untuk method tersebut dan tidak harus memiliki nama yang sama dengan
RequestParameter. Ketika anda membuka localhost:8080 anda akan melihat isi dari home.html

## Adventurer Power Level

Setelah memperlihatkan bagaimana spring bekerja, guild master juga memperlihatkan sebuah aplikasi yang dahulu digunakan
untuk menghitung kekuatan seorang adventurer di guild. Guild master berharap dengan memperlihatkan aplikasi tua tersebut, ingatan anda
akan semakin kembali.

```java
@Controller
public class AdventurerController {

    @Autowired
    private AdventurerCalculatorService adventurerCalculatorService;

    @RequestMapping("/adventurer/countPower")
    private String showAdventurerPowerFromBirthYear(@RequestParam("birthYear")int birthYear, Model model) {
        model.addAttribute("power", adventurerCalculatorService.countPowerPotensialFromBirthYear(birthYear));
        return "calculator";
    }
}
```
Ada beberapa kata di sana yang membangkitkan ingatan anda. Kata service dan Autowired. 

Anda melihat bahwa isi dari folder service terdiri dari interface `AdventurerCalculatorService` dan class yang melakukan implements interface
tersebut `AdventurerCalculatorServiceImpl` 

Anda melihat `AdventurerCalculatorService` hanya interface biasa. Namun, pada `AdventurerCalculatorServiceImpl` anda melihat anotasi lagi.
 ```java
@Service
public class AdventurerCalculatorServiceImpl implements AdventurerCalculatorService 
```
Class yang melakukan implementasi interface memiliki anotasi `@Service`  Melihat hal itu anda ingat pada tujuan `@Autowired`

`@Autowired` akan mencari secara otomatis kelas yang melakukan implementasi interface yang di-declare. Dengan demikian inisiasi tidak dilakukan
secara manual, tapi menggunakan anotasi ini. Anda menyadari bahwa implementasi ini terdengar familiar bagi anda. 
Namun, saat ini tidak bisa mengingatnya. Guild master mengatakan bahwa ia juga tidak tahu kenapa harus menggunakan anotasi tersebut.
Aplikasi tersebut dibuat oleh seorang pahlawan dahulu kala yang mengajarkan sihir spring kepada mereka. 

Mata anda tertuju pada parameter di controller.Pada aplikasi contoh tadi, anda juga melihat  parameter bernama model. Guild master lalu menjelaskan fungsi parameter tersebut.
Model digunakan untuk passing data ke view, ke file html. Hal itu berarti akan ada variable bernama "power" dengan nilai hasil perhitungan 
fungsi `countPowerPotensialFromBirthYear`. 

```java
@Override
    public int countPowerPotensialFromBirthYear(int birthYear) {
        int rawAge = getRawAge(birthYear);
        if (rawAge<30) {
            return rawAge*2000;
        } else if (rawAge <50) {
            return rawAge*2250;
        } else {
            return rawAge*5000;
        }
    }

    private int getRawAge(int birthYear) {
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        return currentYear-birthYear;
    }
```
Fungsi tadi akan menerima input dan menghitung kekuatan adventurer. Hasil perhitungan tadi akan di kirim ke html. 
```html
<!DOCTYPE html>
<html lang="en"  xmlns:th="http://www.thymeleaf.org">
<head>
    <meta charset="UTF-8">
    <title>Adventurer Calculator</title>
</head>
<body>
<h2>Welcome</h2>
<h3 th:text="${'the power is ' + power}">to the guild</h3>

</body>
</html>
```
Untuk dapat menampilkan data yang sudah dikirim melalui controller di spring, anda akan membutuhkan sihir lain.
Sihir itu dikenal dengan nama template engine. Guild menggunakan thymeleaf sebagai template engine. 
Pemakaian dasarnya seperti yang anda dapat lihat.

Ada deklarasi `xmlns:th="http://www.thymeleaf.org"` untuk mulai menggunakan thymeleaf.

Lalu ada `th:text`. Fungsinya adalah untuk menggantikan teks yang ada pada tag itu dengan apa yang ada pada `th:text`

Untuk memanggil variable yang diberikan controller, secara umum dilakukan dengan cara
`th:[th operation ex:text]="${nama-variable}"`

Untuk dapat memasukkan string lain dapat dilakukan dengan
`th:[th operation]="${'other string' + nama-variable}"`

Anda lalu merasa ingin tahu bagaimana bentuk html pada aplikasi dasar sebelumnya. 
```html
<!DOCTYPE html>
<html lang="en" xmlns:th="http://www.thymeleaf.org">
<head>
    <meta charset="UTF-8">
    <title>Welcome</title>
</head>
<body>
<h1>Welcome</h1>
<h2 th:if="${name !=null}" th:text="${'Hello ' + name}"></h2>
</body>
</html>
```

Anda melihat `th:if`. Guild master menjelaskan bahwa itu adalah penggunaan conditional.
Apabila kondisi dalam `th:if` terpenuhi, barulah tag tersebut dibuat. Dengan pengetahuan tersebut anda ingin iseng
terhadap sistem lama yang sudah tidak dipakai. Guild master mengizinkan anda untuk mengutak-atik sistem lama tersebut.

Anda merasa angka tersebut kurang bermanfaat sehingga butuh sedikit perubahan agar lebih bermakna. 
1. Apabila kekuatannya antara 0 - 20000, tambahkan teks 'C class' disamping power di html
2. Apabila kekuatannya antara 20000 - 100000 tambahkan teks 'B class' .
3. Apabila kekuatannya lebih dari 100000, tambahkan teks 'A class'

Agar rapi anda merasa, yang melakukan klasifikasi adalah spring. Thymeleaf akan digunakan untuk menampilkan data saja.

## The Art of Git


## Checklist

- [ ] Membaca tutorial ini
- [ ] Membuat implementasi powerClassifier pada bagian service
- [ ] Membuat perubahan agar controller dan html (thymeleaf) dapat menampilkan data 