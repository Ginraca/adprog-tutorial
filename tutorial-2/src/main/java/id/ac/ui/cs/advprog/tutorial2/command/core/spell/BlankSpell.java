package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

public class BlankSpell implements Spell {

    @Override
    public void cast() {
        // cast nothing
        System.out.println("Eak gabisa");
    }

    @Override
    public void undo() {
        // undo nothing
        System.out.println("Eak gaada yang diundo");
    }

    @Override
    public String spellName() {
        return "Blank Spell";
    }

}
