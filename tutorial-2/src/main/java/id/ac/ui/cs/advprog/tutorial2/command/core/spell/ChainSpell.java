package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.Familiar;
import java.util.*;

public class ChainSpell implements Spell {
    // TODO: Complete Me
    protected Familiar familiar;
    protected ArrayList<Spell> listSpell;

    public ChainSpell(ArrayList<Spell> listSpell) {
        this.listSpell = listSpell;
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }

    public void cast() {
        for (int i = 0; i < listSpell.size(); i++) {
            listSpell.get(i).cast();
        }
    }

    public void undo() {
        for (int i = listSpell.size() - 1; i >= 0; i--) {
            listSpell.get(i).cast();
        }
    }
}
