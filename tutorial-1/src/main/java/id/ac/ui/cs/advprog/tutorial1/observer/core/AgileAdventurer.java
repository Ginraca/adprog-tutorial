package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class AgileAdventurer extends Adventurer {

        public AgileAdventurer(Guild guild) {
                this.name = "Agile";
                //ToDo: Complete Me
                this.guild = guild;
                this.guild.add(this);
        }

        //ToDo: Complete Me
        public void update() {
                if (this.guild.getQuestType().equals("R") || this.guild.getQuestType().equals("D"))
                        this.getQuests().add(this.guild.getQuest());              
        }
}
