package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithMagic implements AttackBehavior {
        //ToDo: Complete me
        public String attack() {
                return "Wingardium Leviosa";
        }
        
        public String getType() {
                return "Attack with Magic";
        }
}
