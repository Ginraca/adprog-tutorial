package id.ac.ui.cs.advprog.tutorial1.strategy.service;

import id.ac.ui.cs.advprog.tutorial1.strategy.core.*;
import id.ac.ui.cs.advprog.tutorial1.strategy.repository.AdventurerRepository;
import id.ac.ui.cs.advprog.tutorial1.strategy.repository.StrategyRepository;
import org.springframework.stereotype.Service;

@Service
public class AdventurerServiceImpl implements AdventurerService {

    private final AdventurerRepository adventurerRepository;

    private final StrategyRepository strategyRepository;

    public AdventurerServiceImpl(AdventurerRepository adventurerRepository,
                                 StrategyRepository strategyRepository) {

        this.adventurerRepository = adventurerRepository;
        this.strategyRepository = strategyRepository;
    }

    @Override
    public Iterable<Adventurer> findAll() {
        return adventurerRepository.findAll();
    }

    @Override
    public Adventurer findByAlias(String alias) {
        return adventurerRepository.findByAlias(alias);
    }

    @Override
    public void changeStrategy(String alias, String attackType, String defenseType) {
        //ToDo: Complete me
        Adventurer adventurer = findByAlias(alias);
        
        if (attackType.equals("Sword")) adventurer.setAttackBehavior(new AttackWithSword());
        else if (attackType.equals("Magic")) adventurer.setAttackBehavior(new AttackWithMagic());
        else adventurer.setAttackBehavior(new AttackWithGun());

        if (defenseType.equals("Armor")) adventurer.setDefenseBehavior(new DefendWithArmor());
        else if (defenseType.equals("Barrier") adventurer.setDefenseBehavior(new DefendWithBarrier());
        else adventurer.setDefenseBehavior(new DefendWithShield());
    }

    @Override
    public Iterable<AttackBehavior> getAttackBehaviors() {
        return strategyRepository.getAttackBehaviors();
    }

    @Override
    public Iterable<DefenseBehavior> getDefenseBehaviors() {
        return strategyRepository.getDefenseBehaviors();
    }
}
