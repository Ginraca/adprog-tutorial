package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithShield implements DefenseBehavior {
        //ToDo: Complete me
        public String defend() {
                return "Protected by Shield";
        }

        public String getType() {
                return "Defend With Shield";
        }
}
