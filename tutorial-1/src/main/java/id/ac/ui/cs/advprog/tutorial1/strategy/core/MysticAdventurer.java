package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class MysticAdventurer extends Adventurer {
        //ToDo: Complete me
        // private AttackBehavior attackBehavior;

        public MysticAdventurer() {
                setAttackBehavior(new AttackWithMagic());
                setDefenseBehavior(new DefendWithShield());
                // this.attackBehavior = new AttackWithMagic();
                // this.defenseBehavior = new DefendWithShield();
        }

        public String getAlias() {
                return "Mystic";
        }
}
