package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class KnightAdventurer extends Adventurer {
        //ToDo: Complete me
        // private AttackBehavior attackBehavior;
        // private DefenseBehavior defenseBehavior;
        
        public KnightAdventurer() {
                setAttackBehavior(new AttackWithSword());
                setDefenseBehavior(new DefendWithArmor());
                // this.attackBehavior = new AttackWithSword();
                // this.defenseBehavior = new DefendWithArmor();
        }

        public String getAlias() {
                return "Knight";
        }
}
