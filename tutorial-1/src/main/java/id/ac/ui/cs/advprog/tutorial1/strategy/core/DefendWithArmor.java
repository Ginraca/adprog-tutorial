package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithArmor implements DefenseBehavior {
        //ToDo: Complete me
        public String defend() {
                return "Protected by Armor";
        }

        public String getType() {
                return "Defend With Armor";
        }
}
